import Client from '../../dev/http-client';
import type { CatMinInfo, CatsList } from '../../dev/types';
import { matchers } from 'jest-json-schema';
expect.extend(matchers);

const cats: CatMinInfo[] = [
  { name: 'Куперавтотестер', description: '', gender: 'male' }, 
  { name: 'Луперавтотестер', description: '', gender: 'male' }, 
  { name: 'Руперавтотестер', description: '', gender: 'male' }];

let cuperCatId;
let luperCatId;
let ruperCatId;
let invalidCatId = 'noOneKnowsThisCat'
let catDescription = 'noOneKnowsThisCat'

const schema = require('./schema.json');
 

const HttpClient = Client.getInstance();

describe('Домашнее задание', () => {
  beforeAll(async () => {
    try {
      const add_cat_response = await HttpClient.post('core/cats/add', {
        responseType: 'json',
        json: { cats },
      });
      if ((add_cat_response.body as CatsList).cats[0].id && (add_cat_response.body as CatsList).cats[1].id) {
        cuperCatId = (add_cat_response.body as CatsList).cats[0].id;     
        luperCatId = (add_cat_response.body as CatsList).cats[1].id;     
        ruperCatId = (add_cat_response.body as CatsList).cats[2].id;   
      } else throw new Error('Не получилось получить id тестового котика!');
    } catch (error) {
      throw new Error('Не удалось создать котика для автотестов!');
    }        
    await HttpClient.delete(`core/cats/${luperCatId}/remove`, {
      responseType: 'json',
    });
  });

  afterAll(() => {
    [cuperCatId, ruperCatId].forEach(async (value) => {
      await HttpClient.delete(`core/cats/${value}/remove`, {
        responseType: 'json',
      });
    })
  });

  it('Поиск существующего кота', async () => {
    const response = await HttpClient.get(`core/cats/get-by-id?id=${cuperCatId}`, {
      responseType: 'json',
    });
    expect(response.statusCode).toEqual(200);

    expect(response.body).toEqual({
      cat: {
        id: cuperCatId,
        ...cats[0],
        tags: null,
        likes: 0,
        dislikes: 0,
      }
    });
  });

  it('Поиск кота по некорректному ID', async () => {
    try {
      const response = await HttpClient.get(`core/cats/get-by-id?id=${invalidCatId}`, {
        responseType: 'json',
      });
    } catch (error) {
      expect(error.response.statusCode).toEqual(400); 

      expect(error.response.body).toEqual({
        data: null,
        isBoom: true,
        isServer: false,
        output: {
          statusCode: 400,
          payload: {
            statusCode: 400,
            error: 'Bad Request',
            message: 'Буквы в ID не принимаются'
          },
          headers: {}
        }
      });
    } 
  });

  it('Поиск несуществующего кота', async () => {
    try {
      const response = await HttpClient.get(`core/cats/get-by-id?id=${luperCatId}`, {
        responseType: 'json',
      });
    } catch (error) {
      expect(error.response.statusCode).toEqual(404); 

      expect(error.response.body).toEqual({
        data: null,
        isBoom: true,
        isServer: false,
        output: {
          statusCode: 404,
          payload: {
            statusCode: 404,
            error: 'Not Found',
            message: `Кот с id=${luperCatId} не найден`
          },
          headers: {}
        }
      });
    } 
  });

  it('Добавление описания коту', async () => {
    const response = await HttpClient.post(`core/cats/save-description`, {
      responseType: 'json',
      json: {
        catId: `${ruperCatId}`,
        catDescription: `${catDescription}`,
      },
    });
    expect(response.statusCode).toEqual(200);
    cats[2].description = catDescription;

    expect(response.body).toEqual({      
      id: ruperCatId,
      ...cats[2],
      tags: null,
      likes: 0,
      dislikes: 0,
    });
  });

  it('Получение списка котов', async () => {
    const response = await HttpClient.get(`core/cats/allByLetter`, {
      responseType: 'json',
    });
    expect(response.statusCode).toEqual(200);

    expect(response.body).toMatchSchema(schema);
  });
});
